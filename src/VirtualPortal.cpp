#include "VirtualPortal.h"
#include "Arduboy.h"
extern Arduboy arduboy;

VirtualPortal::VirtualPortal() {
}

void VirtualPortal::begin() {
}

int VirtualPortal::respondTo(uint8_t* message, uint8_t* response) {
  CommandHeader *ch = (CommandHeader *)message;
  uint8_t chksum = message[ch->length+2];
  int responseSize = 0;

  if (ch->literal != 'U') {
    arduboy.setCursor(0, LINE_ERROR);
    arduboy.print("Non-U command");
    return 0;
  }

  if (chksum != checksum(message)) {
    arduboy.setCursor(0, LINE_ERROR);
    arduboy.print("Warning: bad checksum");
    return 0;
  }

  switch(ch->command) {
    case WAKE:
      responseSize = wake(message, response);
      break;
    case SEED:
      responseSize = seed(message, response);
      break;
    case CHAL:
      responseSize = challenge(message, response);
      break;
    case READ:
      return read(message, response);
    case MODEL:
      return model(message, response);
    case FADE:
      return fade(message, response);
    default:
      responseSize = ack(message, response);
      break;
  }

  return responseSize;
}

void VirtualPortal::encrypt(uint32_t* value) {
  TEA::encrypt(value, key);
}

void VirtualPortal::decrypt(uint32_t* value) {
  TEA::decrypt(value, key);
}

int VirtualPortal::responseFrame(const uint8_t* message, const uint8_t* content, uint8_t contentLength, uint8_t* response) {
  response[0] = 'U';
  response[1] = contentLength + 1;
  response[2] = message[3];
  memcpy(response+3, content, contentLength);
  response[3+contentLength] = checksum(response);
  return contentLength + 4;
}

int VirtualPortal::challenge(uint8_t* message, uint8_t* response) {
  uint32_t xy[2] = {0};
  uint8_t contentLength = sizeof(xy);
  memcpy(xy, message+4, contentLength);
  decrypt(xy);

  xy[1] = xy[0];
  xy[0] = burtle->value();
  encrypt(xy);

  return responseFrame(message, (uint8_t*)xy, contentLength, response);
}

int VirtualPortal::seed(uint8_t* message, uint8_t* response) {
  uint32_t xy[2] = {0};
  uint8_t contentLength = sizeof(xy);
  memcpy(xy, message+4, contentLength);
  decrypt(xy);

  burtle = new Burtle(xy[0]);
  xy[0] = xy[1];
  xy[1] = 0;
  encrypt(xy);

  return responseFrame(message, (uint8_t*)xy, contentLength, response);
}

int VirtualPortal::read(uint8_t* message, uint8_t* response) {
  uint8_t nfcIndex = message[PARAMETERS_START+0];
  uint8_t page = message[PARAMETERS_START+1];
  Token *t = active[nfcIndex];
  if (t && page == 0x26) {
    uint8_t contents[17] = {
      SUCCESS,
      0x00, t->type, 0x00, 0x00,
      0x00, 0x00, 0x00, 0x00,
      0x60, 0x0c, 0x3f, 0xbd,
      0x04, 0x00, 0x00, 0x1e
    };
    return responseFrame(message, contents, sizeof(contents), response);
  }
  return 0;
}

int VirtualPortal::wake(uint8_t* message, uint8_t* response) {
  const uint8_t contents[] = {
    0x00, 0x2f, 0x02, 0x01, 0x02, 0x02, 0x04, 0x02, 0xf5, 0x00, 0x19, 0x45, 0x53, 0xdc, 0x2d, 0xee, 0xae, 0x4e, 0x98, 0xc9, 0x04, 0x02, 0x30, 0x34
  };
  uint8_t contentLength = sizeof(contents);

  return responseFrame(message, contents, contentLength, response);
}

int VirtualPortal::ack(uint8_t* message, uint8_t* response) {
  return responseFrame(message, NULL, 0, response);
}

int VirtualPortal::fade(uint8_t* message, uint8_t* response) {
  uint8_t *parameters = message + PARAMETERS_START;
  Platform p = (Platform)parameters[0];
  LED led;
  uint8_t s = parameters[1];
  uint8_t a = parameters[2];
  led.red = parameters[3];
  led.green = parameters[4];
  led.blue = parameters[5];
  updateLed(p, led);
  return ack(message, response);
}

void VirtualPortal::updateLed(Platform p, LED led) {
  leds[p] = led;
  if (p == ALL) {
    for(int i = 1; i < 4; i++) {
      leds[i] = led;
    }
  }
}

int VirtualPortal::model(uint8_t* message, uint8_t* response) {
  uint8_t contents[9] = {0};
  uint8_t contentLength = sizeof(contents);
  memcpy(contents + 1, message + PARAMETERS_START, contentLength - 1);
  decrypt((uint32_t*)(contents + 1));
  uint8_t nfcIndex = contents[1];
  Token *token = active[nfcIndex];
  if (token) {
    contents[0] = SUCCESS;
    contents[1] = token->character.id;
    contents[2] = 0;
    contents[3] = 0;
    contents[4] = 0;
  } else {
    contents[0] = BADINDEX;
  }

  encrypt((uint32_t*)(contents + 1));
  return responseFrame(message, contents, contentLength, response);
}

int VirtualPortal::place(Token *t, uint8_t index, uint8_t* buffer) {
  active[index] = t;

  Update update;
  update.pad = padForIndex(index);
  update.nfcIndex = index;
  update.direction = ARRIVING;
  if (t->type == CHARACTER) {
    update.uid[5] = t->character.id;
  } else if (t->type == VEHICLE) {
    update.uid[4] = t->vehicle.id << 8;
    update.uid[5] = t->vehicle.id & 0xff;
  }
	update.checksum = checksum((uint8_t*)&update);
  memcpy(buffer, &update, sizeof(update));
	return sizeof(update);
}

Platform VirtualPortal::padForIndex(uint8_t index) {
  switch(index) {
    case 0: return LEFT;
    case 1: return LEFT;
    case 2: return LEFT;
    case 3: return CENTER;
    case 4: return RIGHT;
    case 5: return RIGHT;
    case 6: return RIGHT;
    default: return ALL;
  }
  return ALL;
}

int VirtualPortal::remove(uint8_t index, uint8_t* buffer) {
  Token *t = active[index];
  active[index] = NULL;

  Update update;
  update.pad = padForIndex(index);
  update.nfcIndex = index;
  update.direction = DEPARTING;
  if (t->type == CHARACTER) {
    update.uid[5] = t->character.id;
  } else if (t->type == VEHICLE) {
    update.uid[4] = t->vehicle.id << 8;
    update.uid[5] = t->vehicle.id & 0xff;
  }
	update.checksum = checksum((uint8_t*)&update);
  memcpy(buffer, &update, sizeof(update));
	return sizeof(update);
}

uint8_t VirtualPortal::checksum(uint8_t* buf) {
  uint8_t len = buf[1] + 2; // +2 to include 'U' and length
  int sum = 0;
  for(int i = 0; i < len; i++) {
    sum += buf[i];
  }
  return sum & 0xFF;
}

Token* VirtualPortal::atIndex(int nfcIndex) {
  return active[nfcIndex];
}

String VirtualPortal::commandName(uint8_t cmd) {
  switch(cmd) {
    case WAKE:
      return "WAKE";
    case SEED:
      return "SEED";
    case CHAL:
      return "CHAL";
    case COL:
      return "COLOR";
    case GETCOL:
      return "GETC";
    case FADE:
      return "FADE";
    case FLASH:
      return "FLASH";
    case FADRD:
      return "FADERD";
    case MIRROR:
      return "MIRROR";
    case FADAL:
      return "FADEALL";
    case FLSAL:
      return "FLASHALL";
    case COLAL:
      return "COLORALL";
    case TGLST:
      return "TAGLIST";
    case READ:
      return "READ";
    case WRITE:
      return "WRITE";
    case MODEL:
      return "MODEL";
    case PWD:
      return "PWD";
    case ACTIVE:
      return "ACTIVE";
    default:
      return String(cmd, HEX);
  }
}

String VirtualPortal::asHex(uint8_t num) {
  char tmp[3];
  sprintf(tmp, "%02x", num);
  return String(tmp);
}

String VirtualPortal::padName(Platform p) {
  switch(p) {
    case ALL: return "ALL";
    case LEFT: return "L__";
    case CENTER: return "_C_";
    case RIGHT: return "__R";
  }
  return "";
}
