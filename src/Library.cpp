
#include "Library.h"

Library::Library() : currentIndex(0) {
  tokenCount = ThePoster::characterCount + ThePoster::vehicleCount;
  shelf = new Token[tokenCount];

  for(int i = 0; i < ThePoster::characterCount; i++) {
    shelf[i].type = CHARACTER;
    shelf[i].character.id = CHARACTER_BASE_INDEX + i;
  }

  for(int i = 0; i < ThePoster::vehicleCount; i++) {
    int index = ThePoster::characterCount + i;
    shelf[index].type = VEHICLE;
    shelf[index].vehicle.id = VEHICLE_BASE_INDEX + i;
  }

}

Token* Library::curr() {
  return &shelf[currentIndex];
}

Token* Library::next() {
  Token *next;
  do {
    currentIndex = positive_modulo(currentIndex + 1, tokenCount);
    next = &shelf[currentIndex];
  } while (isHidden(next));
  return next;
}

Token* Library::prev() {
  Token *prev;
  do {
    currentIndex = positive_modulo(currentIndex - 1, tokenCount);
    prev = &shelf[currentIndex];
  } while (isHidden(prev));
  return prev;
}

void Library::hide(Token *token) {
  for (int i = 0; i < MAX_HIDDEN; i++) {
    if (hidden[i] == NULL) {
      hidden[i] = token;
      return;
    }
  }
}

bool Library::isHidden(Token *token) {
  for (int i = 0; i < MAX_HIDDEN; i++) {
    if (hidden[i] == token) {
      return true;
    }
  }
  return false;
}

void Library::show(Token *token) {
  for (int i = 0; i < MAX_HIDDEN; i++) {
    if (hidden[i] == token) {
      hidden[i] = NULL;
      return;
    }
  }
}


inline int Library::positive_modulo(int i, int n) {
  return (i % n + n) % n;
}
