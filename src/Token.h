
#pragma once

enum BrickDesign {
  CHARACTER = 0, VEHICLE = 1
};

struct Character {
  uint8_t id;
};

struct Vehicle {
  uint16_t id;
  uint32_t upgrades0x23;
  uint32_t upgrades0x25;
};

struct Token {
  BrickDesign type;
  union {
    Character character;
    Vehicle vehicle;
  };
};

