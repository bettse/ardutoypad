
#include "Burtle.h"

Burtle::Burtle(uint32_t seed) : a(0xf1ea5eed), startingIterations(42) {
  b = c = d = seed;
  for (int i = 0; i < startingIterations; i++) {
    value();
  }
}

uint32_t Burtle::value() {
  uint32_t e = a - rot(b, 21);
  a = b ^ rot(c, 19);
  b = c + rot(d, 6);
  c = d + e;
  d = e + a;
  return d;
}

uint32_t Burtle::rot(uint32_t x, uint32_t k) {
  return ( x << k ) | ( x >> (32 - k) );
}
