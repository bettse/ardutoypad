
#pragma once

#include <stdint.h>
#include "Arduino.h"
#include "Token.h"
#include "ThePoster.h"

// Number of tokens that can be on toypad
#define MAX_HIDDEN 7

class Library {
  public:
    Library();
    Token* next();
    Token* curr();
    Token* prev();
    void hide(Token* token);
    void show(Token* token);
  private:
    uint32_t currentIndex;
    Token *shelf;
    Token *hidden[MAX_HIDDEN];
    int tokenCount;
    inline int positive_modulo(int i, int n);
    bool isHidden(Token *token);
};
