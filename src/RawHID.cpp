/*
Copyright (c) 2014-2015 NicoHood
See the readme for credit to other people.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

#include "RawHID.h"
#include "Arduboy.h"
extern Arduboy arduboy;

static const uint8_t _hidReportDescriptorRawHID[] PROGMEM = {
		0x06, 0x00, 0xFF, // Usage page
		0x09, 0x01,       // Usage
		0xa1, 0x01,       // Collection
		0x19, 0x01, // usage min
		0x29, 0x20, // usage max
		0x15, 0x00, // Logical min
		0x26, 0xff, 0x00, // logical max
		0x75, 0x08,  // report size
		0x95, 0x20, // report count
		0x81, 0x00, // input
		0x19, 0x01, // usage min
		0x29, 0x20, // usage max
		0x91, 0x00, // output
		0xc0  // end collection
};

const DeviceDescriptor USB_DeviceDescriptor = D_DEVICE(0x00,0x00,0x00,64,0x0E6F,0x0241,0x0100,IMANUFACTURER,IPRODUCT,ISERIAL,1);

typedef struct {
  InterfaceDescriptor hid;
  HIDDescDescriptor   desc;
  EndpointDescriptor  in;
  EndpointDescriptor  out;
} HIDDescriptorbi;


RawHID_::RawHID_(void) : PluggableUSBModule(2, 1, epType), protocol(HID_REPORT_PROTOCOL), idle(0) {
  epType[0] = EP_TYPE_INTERRUPT_IN;
  epType[1] = EP_TYPE_INTERRUPT_OUT;
  PluggableUSB().plug(this);
}

int RawHID_::getInterface(uint8_t *interfaceCount) {
  // Maybe as optional device FastRawHID with different USAGE PAGE
  *interfaceCount += 1; // uses 1
  HIDDescriptorbi hidInterface = {
      D_INTERFACE(pluggedInterface, sizeof(epType), USB_DEVICE_CLASS_HUMAN_INTERFACE, HID_SUBCLASS_NONE, HID_PROTOCOL_NONE),
      D_HIDREPORT(sizeof(_hidReportDescriptorRawHID)),
      D_ENDPOINT(USB_ENDPOINT_IN(pluggedEndpoint), USB_ENDPOINT_TYPE_INTERRUPT, RAWHID_SIZE, 10),
      D_ENDPOINT(USB_ENDPOINT_OUT(pluggedEndpoint+1), USB_ENDPOINT_TYPE_INTERRUPT, RAWHID_SIZE, 10)
  };
  return USB_SendControl(0, &hidInterface, sizeof(hidInterface));
}

int RawHID_::getDescriptor(USBSetup &setup) {

  if (setup.wValueH == USB_DEVICE_DESCRIPTOR_TYPE) {
    const u8* addr = (const u8*)&USB_DeviceDescriptor;
    u8 length = sizeof(USB_DeviceDescriptor);
    return USB_SendControl(0, addr, length);
  }

  // Check if this is a HID Class Descriptor request
  if (setup.bmRequestType != REQUEST_DEVICETOHOST_STANDARD_INTERFACE) {
    return 0;
  }
  if (setup.wValueH != HID_REPORT_DESCRIPTOR_TYPE) {
    return 0;
  }

  // In a HID Class Descriptor wIndex contains the interface number
  if (setup.wIndex != pluggedInterface) {
    return 0;
  }

  // Reset the protocol on reenumeration. Normally the host should not assume the state of the protocol
  // due to the USB specs, but Windows and Linux just assumes its in report mode.
  protocol = HID_REPORT_PROTOCOL;

  return USB_SendControl(TRANSFER_PGM, _hidReportDescriptorRawHID, sizeof(_hidReportDescriptorRawHID));
}

bool RawHID_::setup(USBSetup &setup) {
  if (setup.wIndex != pluggedInterface) {
    return false;
  }

  uint8_t request = setup.bRequest;
  uint8_t requestType = setup.bmRequestType;

  if (requestType == REQUEST_DEVICETOHOST_CLASS_INTERFACE) {
    if (request == HID_GET_REPORT) {
      // TODO: HID_GetReport();
      return true;
    }
    if (request == HID_GET_PROTOCOL) {
      // TODO: Send8(protocol);
      return true;
    }
  } else if (requestType == REQUEST_HOSTTODEVICE_CLASS_INTERFACE) {
    if (request == HID_SET_PROTOCOL) {
      protocol = setup.wValueL;
      return false;
    }
    if (request == HID_SET_IDLE) {
      idle = setup.wValueL;
      return false;
    }
    if (request == HID_SET_REPORT) {
      // Check if data has the correct length afterwards
      int length = setup.wLength;

      // Feature (set feature report)
      if (setup.wValueH == HID_REPORT_TYPE_FEATURE) {
        return true;
      }

      // Output (set out report)
      else if (setup.wValueH == HID_REPORT_TYPE_OUTPUT) {
        return true;
      }

      // Input (set HID report)
      else if(setup.wValueH == HID_REPORT_TYPE_INPUT) {
        return true;
      }
    }
  }

  return false;
}

RawHID_ RawHID;
