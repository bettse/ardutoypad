/*
Copyright (c) 2014-2015 NicoHood
See the readme for credit to other people.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

// Include guard
#pragma once

#undef USB_PRODUCT
#define USB_PRODUCT "LEGO READER V2.10"
#undef USB_MANUFACTURER
#define USB_MANUFACTURER "PDP LIMITED. "

#include <Arduino.h>
#include "PluggableUSB.h"
#include "HID.h"
#include "HID-Settings.h"

#define LD_INTERFACE 0
#define LD_IN_EP 1
#define LD_OUT_EP 2

#define RAWHID_SIZE 32

class RawHID_ : public PluggableUSBModule, public Stream {
public:
  RawHID_(void);

  virtual int available(void) {
    return USB_Available(pluggedEndpoint+1);
  }

  virtual int peek() {
    return -1;
  }

  virtual int read() {
    if (available() > 0) {
      return USB_Recv(pluggedEndpoint+1);
    }
    return -1;
  }

  virtual void flush(void) {
    USB_Flush(pluggedEndpoint);
  }

  // Wrapper for a single byte
  using Print::write;
  virtual size_t write(uint8_t b) {
    return write(&b, 1);
  }

  virtual size_t write(uint8_t *buffer, size_t size) {
    return USB_Send(pluggedEndpoint | TRANSFER_RELEASE, buffer, size);
  }

protected:
  // Implementation of the PUSBListNode
  int getInterface(uint8_t *interfaceCount);
  int getDescriptor(USBSetup &setup);
  bool setup(USBSetup &setup);

  uint8_t epType[2];
  uint8_t protocol;
  uint8_t idle;
};
extern RawHID_ RawHID;
