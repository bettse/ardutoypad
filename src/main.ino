
#include "RawHID.h"
#include "Arduboy.h"
#include "VirtualPortal.h"
#include "Library.h"
#include "ThePoster.h"

#define HID_MAX_LENGTH 32
#define MAX_PREVIEW 8

#define LINE_WIDTH 20
#define LINE_HEIGHT 8
#define LINE_COMMAND 0 * LINE_HEIGHT
#define LINE_RESPONSE 1 * LINE_HEIGHT
#define LINE_UPDATE 2 * LINE_HEIGHT
#define LINE_LIBRARY 4 * LINE_HEIGHT
#define LINE_ERROR 5 * LINE_HEIGHT
#define RIGHT_HALF WIDTH/2

#define BLINKDELAY 500
#define UI_DEBOUNCE_MS 300

struct FlipFlop {
  long prevMs = 0;
  int period = BLINKDELAY;
  bool state = true;
};

Arduboy arduboy;
VirtualPortal vp = VirtualPortal();
Library library = Library();

unsigned long nextInteractionMs = 0;
FlipFlop uiblink;
Token *displayToken = library.curr();
uint8_t selectedIndex = 0;
bool tokenSelected = false;

void setup() {
  arduboy.beginNoLogo();
  arduboy.setFrameRate(15);
  arduboy.clear();
  arduboy.setCursor(0, 0);
  arduboy.setTextWrap(false);
  vp.begin();
}

void loop() {
  unsigned long currentMillis = millis();
  usbReplies();

  if(currentMillis > nextInteractionMs) {
    // userInteraction() will return a delay if there was interaction
    // to debounce
    nextInteractionMs = currentMillis + userInteraction();
  }

  //TODO: set RGB based on latest FADE/Flash
  LED led = vp.leds[ALL];
  arduboy.setRGBled(led.red, led.green, led.blue);

  updateUI();
}

void updateUI() {
  clearLine(LINE_LIBRARY);
  arduboy.setCursor(0, LINE_LIBRARY);
  if (tokenSelected) {
    arduboy.print(">");
  } else {
    blink(">");
  }
  arduboy.print(ThePoster::getName(displayToken));

  onToypad();
  arduboy.display();
}

void onToypad() {
  for(int i = 0; i < MAX_TOKENS; i++) {
    String name = "_________";
    Token *t = vp.atIndex(i);
    arduboy.setCursor(RIGHT_HALF, i*LINE_HEIGHT);
    arduboy.print(i);
    arduboy.print(":");

    if (t) {
      name = ThePoster::getName(t);
    }

    if (tokenSelected && selectedIndex == i) {
      blink(name);
    } else {
      arduboy.print(name);
    }

  }
}

uint8_t nextAvailableIndex() {
  return nextAvailableIndex(0);
}

uint8_t nextAvailableIndex(uint8_t start) {
  for(int i = start; i < MAX_TOKENS; i++) {
    if(vp.atIndex(i) == NULL) {
      return i;
    }
  }
  return MAX_TOKENS;
}

void usbReplies() {
  uint8_t incoming[HID_MAX_LENGTH] = {0};
  uint8_t response[HID_MAX_LENGTH] = {0};

  uint8_t usbAvailable = RawHID.available();
  if (usbAvailable > 0) {
    for(int i = 0; i < usbAvailable; i++) {
      incoming[i] = RawHID.read();
    }
    arduboy.setCursor(0, LINE_COMMAND);
    arduboy.print("=>");
    arduboy.print(vp.commandName(incoming[2]));
    uint8_t len = vp.respondTo(incoming, response);
    if (len) {
      RawHID.write(response, HID_MAX_LENGTH);
      RawHID.flush();
    }
  }
}

void placeToken(Token *token, uint8_t index) {
  Token *existing = vp.atIndex(index);
  if (existing) {
    return; //For now, don't handle trying to put a token on top of another token
  }
  uint8_t update[HID_MAX_LENGTH] = {0};
  int updateLength = vp.place(token, index, update);
  library.hide(token);
  displayToken = library.next();
  if (updateLength > 0) {
    RawHID.write(update, HID_MAX_LENGTH);
    RawHID.flush();
  }
}

void removeToken(uint8_t index) {
  Token *existing = vp.atIndex(index);
  if (existing) { // You can only remove what exists
    uint8_t update[HID_MAX_LENGTH] = {0};
    int updateLength = vp.remove(index, update);
    library.show(existing);
    if (updateLength > 0) {
      RawHID.write(update, HID_MAX_LENGTH);
      RawHID.flush();
    }
  }
}

/*
  Up/down to select a token
  Right to switch to selecting a place for it
  Left to cancel without doing anything
  up/down to choose which place
  A: Add to that place
  B: Remove token at that place
  NB: You can't add on top of soemthing that is already there,
   and you can't remove something that doens't exist
*/
int userInteraction() {
  if (arduboy.pressed(A_BUTTON)) {
    if (tokenSelected) {
      placeToken(displayToken, selectedIndex);
      tokenSelected = false;
      return UI_DEBOUNCE_MS;
    }
  } else if (arduboy.pressed(B_BUTTON)) {
    if (tokenSelected) {
      removeToken(selectedIndex);
      tokenSelected = false;
      return UI_DEBOUNCE_MS;
    }
  } else if (arduboy.pressed(UP_BUTTON)) {
    if (tokenSelected) {
      selectedIndex = max(0, selectedIndex-1);
    } else {
      displayToken = library.prev();
    }
    return UI_DEBOUNCE_MS;
  } else if (arduboy.pressed(DOWN_BUTTON)) {
    if (tokenSelected) {
      selectedIndex = min(MAX_TOKENS-1, selectedIndex+1);
    } else {
      displayToken = library.next();
    }
    return UI_DEBOUNCE_MS;
  } else if (arduboy.pressed(RIGHT_BUTTON)) {
    if (!tokenSelected) {
      tokenSelected = true;
      selectedIndex = nextAvailableIndex();
      return UI_DEBOUNCE_MS;
    }
  } else if (arduboy.pressed(LEFT_BUTTON)) {
    if (tokenSelected) {
      tokenSelected = false;
      return UI_DEBOUNCE_MS;
    }
  }
  return 0; //No interaction, no delay
}

void blink(String words) {
  unsigned long currentMillis = millis();

  if(currentMillis - uiblink.prevMs > uiblink.period) {
    uiblink.prevMs = currentMillis;
    uiblink.state = !uiblink.state;
  }

  int len = words.length();
  if (uiblink.state) {
    arduboy.print(words);
  } else {
    // This feels weird
    for (int i = 0; i < len; i++) {
      arduboy.print(" ");
    }
  }
}

void clearLine(int line) {
  arduboy.setCursor(0, line);
  for(int i = 0; i < LINE_WIDTH; i++) {
    arduboy.print(" ");
  }
}

String asHex(uint8_t num) {
  char tmp[3];
  sprintf(tmp, "%02X", num);
  return String(tmp);
}

