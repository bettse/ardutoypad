
#pragma once

#include <stdint.h>

class Burtle {
  public:
    Burtle(uint32_t seed);
    uint32_t value();
  private:
    uint32_t a, b, c, d;
    int startingIterations;
    uint32_t rot(uint32_t x, uint32_t k);
};
