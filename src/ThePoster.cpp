
#include "ThePoster.h"

char ThePoster::nameBuffer[] = {0};

String ThePoster::getName(Token *token) {
  if (token->type == CHARACTER) {
    return characterName(token->character.id);
  } else if (token->type == VEHICLE) {
    return vehicleName(token->vehicle.id);
  }
  return "";
}

String ThePoster::characterName(int id) {
  int index = id - CHARACTER_BASE_INDEX;
  strcpy_P(nameBuffer, (char*)pgm_read_word(&(characterNames[index]))); // Necessary casts and dereferencing, just copy.
  return String(nameBuffer);
}

String ThePoster::vehicleName(int id) {
  int index = id - VEHICLE_BASE_INDEX;
  strcpy_P(nameBuffer, (char*)pgm_read_word(&(vehicleNames[index]))); // Necessary casts and dereferencing, just copy.
  return String(nameBuffer);
}

// http://stackoverflow.com/questions/19047730/clean-and-tidy-string-tables-in-progmem-in-avr-gcc

#define CHARACTER_TABLE \
        ENTRY(Batman) \
        ENTRY(Gandalf) \
        ENTRY(Wyldstyle) \
        ENTRY(Aquaman) \
        ENTRY(Bad_Cop) \
        ENTRY(Bane) \
        ENTRY(Bart) \
        ENTRY(Benny) \
        ENTRY(Chell) \
        ENTRY(Cole) \
        ENTRY(Cragger) \
        ENTRY(Cyborg) \
        ENTRY(Cyberman) \
        ENTRY(Doc_Brown) \
        ENTRY(The_Doctor) \
        ENTRY(Emmet) \
        ENTRY(Eris) \
        ENTRY(Gimli) \
        ENTRY(Smeagol) \
        ENTRY(Harley_Quinn) \
        ENTRY(Homer)
/*
        ENTRY(Jay) \
        ENTRY(Joker) \
        ENTRY(Kai) \
        ENTRY(ACU) \
        ENTRY(Gamer_Kid) \
        ENTRY(Krusty) \
        ENTRY(Laval) \
        ENTRY(Legolas) \
        ENTRY(Lloyd) \
        ENTRY(Marty_Mcfly) \
        ENTRY(Nya) \
        ENTRY(Owen) \
        ENTRY(Peter_Venkman) \
        ENTRY(Slimer) \
        ENTRY(Scooby_Doo) \
        ENTRY(SenseiWu) \
        ENTRY(Shaggy) \
        ENTRY(Stay_Puft) \
        ENTRY(Superman) \
        ENTRY(Unikitty) \
        ENTRY(Wicked_Witch) \
        ENTRY(Wonderwoman) \
        ENTRY(Zane) \
        ENTRY(Green_Arrow) \
        ENTRY(Supergirl)
*/

#define ENTRY(a)    const char CHARACTER_STR_ ## a[] PROGMEM = #a;
CHARACTER_TABLE
#undef ENTRY


const char* const ThePoster::characterNames[] PROGMEM = {
#define ENTRY(a) CHARACTER_STR_ ## a,
    CHARACTER_TABLE
#undef ENTRY
};

// Must be after the array
int ThePoster::characterCount = (sizeof(ThePoster::characterNames) / sizeof(ThePoster::characterNames[0]));


// ** Vehicles ** //

#define VEHICLE_TABLE \
        ENTRY(Police_Car) \
        ENTRY(Aerial_Squad_Car) \
        ENTRY(Missile_Striker) \
        ENTRY(Gravity_Sprinter) \
        ENTRY(Street_Shredder) \
        ENTRY(Sky_Clobberer) \
        ENTRY(Batmobile) \
        ENTRY(Batblaster) \
        ENTRY(Sonic_Batray)

/*
"Police Car"
"Aerial Squad Car"
"Missile Striker"
"Gravity Sprinter"
"Street Shredder"
"Sky Clobberer"
"Batmobile"
"Batblaster"
"Sonic Batray"
"Benny's Spaceship"
"Lasercraft"
"The Annihilator"
"Delorean"
"Ultra Time Machine"
"Electric Time Machine"
"Hoverboard"
"Cyclone Board"
"Ultimate Hoverjet"
"Eagle interceptor"
"Eagle Skyblazer"
"Eagle Swoop Diver"
"Cragger's Fireship"
"Croc Command Sub"
"Swamp Skimmer"
"Cyber Guard"
"Cyber-Wrecker"
"Laser Robot Walker"
"K9"
"K9 Ruff Rover"
"K9 Laser Cutter"
"TARDIS"
"Laser-Pulse TARDIS"
"Energy-Burst TARDIS"
"Emmet's Excavator"
"The Destroydozer"
"Destruct-o-Mech"
"Winged Monkey"
"Battle Monkey"
"Commander Monkey"
"Axe Chariot"
"Axe Hurler"
"Soaring Chariot"
"Shelob the Great"
"8-Legged Stalker"
"Poison Slinger"
"Homer's Car"
"Homercraft"
"SubmaHomer"
"Taunt-o-Vision"
"Blast Cam"
"The MechaHomer"
"Velociraptor"
"Spike Attack Raptor"
"Venom Raptor"
"Gyro Sphere"
"Sonic Beam Gyrosphere"
"Speed Boost Gyrosphere"
"Clown Bike"
"Cannon Bike"
"Anti-Gravity Rocket Bike"
"Mighty Lion Rider"
"Lion Blazer"
"Fire Lion"
"Arrow Launcher"
"Seeking Shooter"
"Triple Ballista"
"Mystery Machine"
"Mystery Tow"
"Mystery Monster"
"Boulder Bomber"
"Boulder Blaster"
"Cyclone Jet"
"Storm Fighter"
"Lightning Jet"
"Electro-Shooter"
"Blade Bike"
"Flying Fire Bike"
"Blades of Fire"
"Samurai Mech"
"Samurai Shooter"
"Soaring Samurai Mech"
"Companion Cube"
"Laser Deflector"
"Gold Heart Emitter"
"Sentry Turret"
"Turret Striker"
"Flying Turret Carrier"
"Scooby Snack"
"Scooby Fire Snack"
"Scooby Ghost Snack"
"Cloud Cukko Car"
"X-Stream Soaker"
"Rainbow Cannon"
"Invisible Jet"
"Stealth Laser Shooter"
"Torpedo Bomber"
"Ninja Copter"
"Glaciator"
"Freeze Fighter"
"Traveling Time Train"
"(Traveling Time Train - rebuilt 1)"
"(Traveling Time Train - rebuilt 2)"
"Aqua Watercraft"
"(Aqua Watercraft - rebuilt 1)"
"(Aqua Watercraft - rebuilt 2)"
"Drill Driver"
"(Drill Driver - rebuilt 1)"
"(Drill Driver - rebuilt 2)"
"Quinn-mobile"
"(Quinn-mobile - rebuilt 1)"
"(Quinn-mobile - rebuilt 2)"
"The Jokers Chopper"
"(The Jokers Chopper - rebuilt 1)"
"(The Jokers Chopper - rebuilt 2)"
"Hover Pod"
"(Hover Pod - rebuilt 1)"
"(Hover Pod - rebuilt 2)"
"Dalek"
"(Dalek - rebuilt 1)"
"(Dalek - rebuilt 2)"
"Ecto-1"
"(Ecto-1 - rebuilt 1)"
"(Ecto-1 - rebuilt 2)"
"Ghost Trap"
"(Ghost Trap - rebuilt 1)"
"(Ghost Trap - rebuilt 2)"
"unknown"
"unknown"
"unknown"
"unknown"
"unknown"
"unknown"
"Llyod's Golden Dragon"
"(Golden Dragon - rebuilt 1)"
"(Golden Dragon - rebuilt 2)"
"unknown"
"unknown"
"unknown"
"unknown"
"unknown"
"unknown"
"unknown"
"unknown"
"unknown"
"Mega Flight Dragon"
"(Mega Flight Dragon - rebuilt 1)"
"(Mega Flight Dragon - rebuilt 2)"
"unknown"
"unknown"
"unknown"
"unknown"
"unknown"
"unknown"
"unknown"
"unknown"
"Flying White Dragon"
"Golden Fire Dragon"
"Ultra Destruction Dragon"
"Arcade Machine"
"8-bit Shooter"
"The Pixelator"
"G-61555 Spy Hunter"
"The Interdiver"
"Aerial Spyhunter"
"Slime Shooter"
"Slime Exploder"
"Slime Streamer"
"Terror Dog"
"Terror Dog Destroyer"
"Soaring Terror Dog"
"Ancient Psychic Tandem War Elephant"
"Lumpy Car"
*/

#define ENTRY(a)    const char VEHICLE_STR_ ## a[] PROGMEM = #a;
VEHICLE_TABLE
#undef ENTRY

const char* const ThePoster::vehicleNames[] PROGMEM = {
#define ENTRY(a) VEHICLE_STR_ ## a,
    VEHICLE_TABLE
#undef ENTRY
};

int ThePoster::vehicleCount = (sizeof(ThePoster::vehicleNames) / sizeof(ThePoster::vehicleNames[0]));
