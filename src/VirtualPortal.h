#ifndef VirtualPortal_h
#define VirtualPortal_h

#include "Arduino.h"
#include "tea.h"
#include "burtle.h"
#include "Token.h"

#define MAX_TOKENS 7
#define PARAMETERS_START 4

#define LINE_HEIGHT 8
#define LINE_ERROR 5 * LINE_HEIGHT

enum ModelStatus {
  SUCCESS = 0x00,
  BADINDEX = 0xf2,
  REMOVED = 0xf0,
  INVALIDTYPE = 0xf9
};

enum SAK {
	NTAG21X = 0
};

enum Direction {
	ARRIVING = 0, DEPARTING = 1
};

enum Platform {
	ALL = 0, CENTER = 1, LEFT = 2, RIGHT = 3
};

enum CommandCode {
  WAKE  = 0xB0,
  SEED  = 0xB1,
  CHAL  = 0xB3,
  COL   = 0xC0,
  GETCOL= 0xC1,
  FADE  = 0xC2,
  FLASH = 0xC3,
  FADRD = 0xC4,
  MIRROR= 0xC5,
  FADAL = 0xC6,
  FLSAL = 0xC7,
  COLAL = 0xC8,
  TGLST = 0xD0,
  READ  = 0xD2,
  WRITE = 0xD3,
  MODEL = 0xD4,
  PWD   = 0xE1,
  ACTIVE= 0xE5
};

struct CommandHeader {
  char literal = 'U';
  uint8_t length;
  uint8_t command;
  uint8_t corrolation;
}__attribute__((packed));


struct ResponseHeader {
  char literal = 'U';
  uint8_t length;
  uint8_t corrolation;
}__attribute__((packed));


struct Update {
  char literal = 'V';
  uint8_t length = 0x0b;
  uint8_t pad;
  uint8_t sak = NTAG21X;
  uint8_t nfcIndex = 0;
  uint8_t direction;
  uint8_t uid[7] = {0x04, 0xCA, 0xFE, 0xDE, 0xED, 0x00, 0x80};
  uint8_t checksum;
}__attribute__((packed));

struct LED {
  uint8_t red = 0;
  uint8_t green = 0;
  uint8_t blue = 0;
};

class VirtualPortal {
  public:
    VirtualPortal();
    LED leds[4]; // 4 = size of 'Platform' enum

    void begin();
    int respondTo(uint8_t *message, uint8_t *response);
    String commandName(uint8_t cmd);
    int place(Token *t, uint8_t index, uint8_t* buffer);
    int remove(uint8_t index, uint8_t* buffer);
    Token* atIndex(int nfcIndex);
    String padName(Platform p);

  private:
    Burtle *burtle;
    Token *active[MAX_TOKENS] = {NULL};
    uint32_t key[4] = {0x30f6fe55, 0xc10bbf62, 0x347cb3c9, 0xfb293e97};
    int wake(uint8_t* message, uint8_t* response);
    int seed(uint8_t* message, uint8_t* response);
    int challenge(uint8_t* message, uint8_t* response);
    int read(uint8_t* message, uint8_t* response);
    int model(uint8_t* message, uint8_t* response);
    int fade(uint8_t* message, uint8_t* response);
    int ack(uint8_t* message, uint8_t* response);
    int responseFrame(const uint8_t* message, const uint8_t* content, uint8_t contentLength, uint8_t* response);

    uint8_t checksum(uint8_t* buf);
    void encrypt(uint32_t* value);
    void decrypt(uint32_t* value);
    void updateLed(Platform p, LED led);
    String asHex(uint8_t num);
    Platform padForIndex(uint8_t index);
};

#endif
