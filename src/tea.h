
#pragma once

#include <stdint.h>

class TEA {
  public:
    static void encrypt (uint32_t* v, uint32_t* k);
    static void decrypt (uint32_t* v, uint32_t* k);
};
