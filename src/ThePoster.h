
#pragma once

#include <stdint.h>
#include "Arduino.h"
#include "Token.h"
#include <avr/pgmspace.h>

#define CHARACTER_BASE_INDEX 1
#define VEHICLE_BASE_INDEX 1000
#define LONGEST_NAME 20

class ThePoster {
  public:
    static const char* const characterNames[] PROGMEM;
    static const char* const vehicleNames[] PROGMEM;
    static int characterCount;
    static int vehicleCount;
    static String getName(Token *token);
  private:
    static String characterName(int id);
    static String vehicleName(int id);
    static char nameBuffer[LONGEST_NAME];
};
